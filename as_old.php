<?php
//index.php

$error = '';
$ekey = '';
$dkey = '';


function clean_text($string)
{
 $string = trim($string);
 $string = stripslashes($string);
 $string = htmlspecialchars($string);
 return $string;
}

if(isset($_POST["submit"]))
{
 if(empty($_POST["ekey"]))
 {
  $error .= '<p><label class="text-danger">Please Enter encryption key</label></p>';
 }
 else
 {
  $ekey = sprintf("0x%08X",$_POST["ekey"]);
 
 }
 if(empty($_POST["dkey"]))
 {
  $error .= '<p><label class="text-danger">Please Enter your Email</label></p>';
 }
 else
 {
  $dkey = "0x".clean_text($_POST["dkey"]);
 
 }
 if($error == '')
 {
  $file_open = fopen("test.csv", "w+");
  $no_rows = count(file("test.csv"));
  
  $form_data = array(
   'sr_no'  => $no_rows,
   'ekey'  => $ekey,
   'dkey'  => $dkey,
   
  );
  fputcsv($file_open, $form_data);
  $error = '<label class="text-success">Thank you for contacting us</label>';
  $ekey = '';
  $dkey = '';

 }
}

?>
<!DOCTYPE html>
<html>
 <head>
  <title>How to Store Form data in CSV File using PHP</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
 <body>
  <br />
  <div class="container">
   <h2 align="center">How to Store Form data in CSV File using PHP</h2>
   <br />
   <div class="col-md-6" style="margin:0 auto; float:none;">
    <form method="post">
     <h3 align="center">Contact Form</h3>
     <br />
     <?php echo $error; ?>
     <div class="form-group">
      <label>Encryption Key</label>
      <input type="text" name="ekey" placeholder="Enter Name" class="form-control" value="<?php echo $ekey; ?>" />
     </div>
     <div class="form-group">
      <label>Decyption key</label>
      <input type="text" name="dkey" class="form-control" placeholder="Enter Email" value="<?php echo $dkey; ?>" />
    
     <div class="form-group" align="center">
      <input type="submit" name="submit" class="btn btn-info" value="Submit" />
     </div>
    </form>
	<?php

$handle = fopen("test.csv", "r"); 
	
while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
	$encryptkey = $data[1];
   $decryptkey = $data[2];
}
echo $encryptkey."<br/>".$decryptkey;
fclose($handle);
?>
   </div>
  </div>
 </body>
</html>